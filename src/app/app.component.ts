import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public title = 'Test Spira';
  private showForm: boolean;
  private showFormData: boolean;
  private user: { username: string, email: string };

  onSubmit(registerForm: NgForm) {
    let value = registerForm.value;
    this.user = {
      username: value.username,
      email: value.email
    }
    this.showFormData = true;
    registerForm.resetForm();
  }

  public showFormContainer() {
    this.showForm = true;
  }

  public getShowForm() {
    return this.showForm;
  }

  public getUser() {
    return this.user;
  }

  public getShowFormData() {
    return this.showFormData;
  }
}
